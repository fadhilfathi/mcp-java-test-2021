/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcp.java.test;

import java.util.Scanner;

/**
 *
 * @author Muh. Fadhil Fathi Rz
 */
public class MCPJavaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int [] x = soal1();
    }

    public static int[] soal1() {
        int[] nums = {1, 2, 3, 4, 4};  //contoh array nums
        int[] max = new int[nums.length]; // inisiasi return array
        int tempmax = 0; // temporary nilai terbesar
        int temp = 0; // temporary array ke- pada return array max
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > tempmax) {
                tempmax = nums[i];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] >= tempmax) {
                max[temp] = tempmax;
                temp+=1;
            }
        }
        return max;
    }
}
