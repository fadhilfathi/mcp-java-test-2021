/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcp.java.test;

import java.util.Scanner;

/**
 *
 * @author Muh. Fadhil Fathi Rz
 */
public class MCPJavaTest2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int [] nums = soal2(x);
    }

    public static int[] soal2(int x) {
        int[] nums = {1, 2, 3, 4, 4}; // contoh array nums
        int[] array = new int[nums.length]; // array yang akan di return
        int temp = 0; //temporary array ke- pada return array
        for (int i = 0; i < nums.length; i++) {
            boolean z = true;
            for (int j = 0; j < nums.length; j++) {
                if ((nums[i] / nums[j]) == x){
                    z = false;
                }
            }
            if (z==true){
                array[temp] = nums[i];
                temp += 1;
            }
        }
        return array;
    }
}
